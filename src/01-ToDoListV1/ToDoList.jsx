import React, { useState } from "react";
import "./ToDoList.module.css";
import styles from "./ToDoList.module.css";
import trashIcon from "./trash-icon.png";


const ToDoList = () => {
  const [form, setForm] = useState({
    task: "",
    date: ""
  });

  const [fullList, setFullList] = useState([]);

  const [chgFlag, setchgFlag] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setForm({
      ...form,  
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setFullList((prevFullList) => [
      ...prevFullList,
      {    
        task: e.target.task.value,  
        date:e.target.date.value,
        checked: false
      }
    ]
    );
setForm({
  task: "",
  date: ""  
});

  };

const handleCheckbox = (e) => {
 const auxIndex = parseInt(e.target.name.slice(7));
 let auxList = fullList;
 auxList[auxIndex].checked = e.target.checked;
 setFullList(auxList);  // ESTO NO ME GENERA UN NUEVO RENDER. PORQUE?

 // Modifico un flag para que se renderice nuevamente
 // Aparentemente si el STATE es de un array, tiene que cambiar
 // la longitud del array para que se tome como un cambio????????
 setchgFlag((chgFlag ? false : true));
};

const handleDelButton = (e) => {
  const auxBtnName = e.target.name.slice(6);
  let auxFullList = fullList;
   auxFullList = fullList.filter((element, index) => {
    return (index.toString()) !== auxBtnName;
  });
  setFullList(auxFullList);
};

  return (
    <div className={styles["rootContainer"]}>
    <br />
    <div className={styles["gralContainer"]}>
      < h1 className={styles["titulo"]}>Lista de Tareas</h1>
      <div className={styles["classNewTask"]}>
      <form onSubmit={handleSubmit}>
        <input type="text" name="task" value={form.task} onChange={handleChange} size="50" placeholder="Ingrese una tarea..." required/>
        <span> - </span>
        <input type="date" name="date" value={form.date} onChange={handleChange} />
        <span> - </span>
        <button type="submit">Añadir</button>
      </form>
      </div>
      <br /><br />
<div>
  {fullList.map((element, index) => 
        <>
        <input type="checkbox" name={"checkBx"+index} id={"idCheckBx"+index} onChange={handleCheckbox} checked={element.checked}/>
        <span name={"spanTask"+index} id={"idSpanTask"+index} className={styles[element.checked ? "classTxtTask1" : "classTxtTask2"]}> <b>{element.task}</b> </span>
        <span className={styles[element.checked ? "classTxtTask1" : "classTxtTask2"]}> {element.date}</span>
        {/* <button onClick={handleDelButton} name={"delBtn"+index} id={"idDelBtn"+index}> <img src={trashIcon} alt="TrashBtn" height="20px"/> </button> */}
        <img src={trashIcon} alt="TrashBtn" name={"delBtn"+index} id={"idDelBtn"+index} onClick={handleDelButton} className={styles["classDelBtn"]}/>
        <br /> <br />
        </>

      )}
      </div>

    </div>
    </div>
  );
};
export default ToDoList;
